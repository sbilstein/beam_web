from django.urls import path

from . import views

urlpatterns = [
    path("", views.onboarding_start, name="index"),
    path("onboarding/signup", views.onboarding_signup, name="onboarding_signup"),
    path("onboarding/intake", views.onboarding_intake, name="onboarding_intake"),
    path(
        "onboarding/availability",
        views.onboarding_availability,
        name="onboarding_availability",
    ),
    path("onboarding/checkout", views.onboarding_checkout, name="onboarding_checkout"),
    path("onboarding/done", views.onboarding_done, name="onboarding_done"),
]

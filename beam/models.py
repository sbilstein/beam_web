from django.db import models
from django.contrib.auth.models import User, AbstractUser


class Client(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)


class ClientIntake(models.Model):
    user = models.ForeignKey(Client, on_delete=models.CASCADE)
    # Should list the questions here I think...

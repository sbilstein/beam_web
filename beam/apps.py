from django.apps import AppConfig


class BeamConfig(AppConfig):
    name = "beam"

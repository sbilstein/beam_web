from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from .forms import SignupForm

# Create your views here.
def onboarding_start(request):
    return render(request, "beam/onboarding_start.html")


def onboarding_signup(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            return HttpResponseRedirect(reverse("onboarding_intake"))
        else:
            return render(request, "beam/onboarding_signup.html", {"signup_form": form})
    return render(request, "beam/onboarding_signup.html", {"signup_form": None})


def onboarding_intake(request):
    return render(request, "beam/onboarding_intake.html")


def onboarding_availability(request):
    return render(request, "beam/onboarding_availability.html")


def onboarding_checkout(request):
    return render(request, "beam/onboarding_checkout.html")


def onboarding_done(request):
    return render(request, "beam/onboarding_done.html")

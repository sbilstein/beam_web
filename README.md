### Running Locally

Starting the Django app:

```
python manage.py runserver
```

### Compiling assets

Assets are stored in `beam/static_src`.
Switch to that directory and install the npm deps with `npm ci`.
To update the assets on file changes run the following from the root directory:

```
cd beam/static_src && npm run watch
```

### Deploys

This demo is currently deployed on Google App Engine. Assuming you are properly configured on GAE, deployment is two steps:

```
python manage.py collectstatic
gcloud app deploy
```
